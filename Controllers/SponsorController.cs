using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TestForAbto.Models;

namespace TestForAbto.Controllers
{
    public class SponsorController : Controller
    {
        private readonly ILogger<SponsorController> _logger;
        private readonly SponsorService _sponsorService;

        public SponsorController(ILogger<SponsorController> logger)
        {
            _sponsorService = new SponsorService();
            _logger = logger;
        }

        public IActionResult Sponsor(int id)
        {
            var sponsorInfo = _sponsorService.GetSponsorInfo(id);
            return View(sponsorInfo);
        }

		public IActionResult Update(SponsorModel sponsor)
		{
			_sponsorService.Update(sponsor);
			return RedirectToAction("Sponsor", new { id = sponsor.ID });
		}

		public IActionResult Index()
        {
            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
