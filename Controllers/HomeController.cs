﻿using Microsoft.AspNetCore.Mvc;

namespace TestForAbto.Controllers
{
    public class HomeController : Controller
    {
        private SponsorService _sponsorService = new();
        public IActionResult Index()
        {
            var SponsorIDsList = _sponsorService.GetSponsorsID().OrderBy(x => x).ToList();
            return View(SponsorIDsList);
        }
    }
}
