﻿namespace TestForAbto.Models
{
    public class SponsorModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public string Note { get; set; }
        public List<int>? Children { get; set; }

        public string GetChildrenList()
        {
            if (Children is null)
                return "";

            return string.Join('\n', Children);
        }
    }
}
