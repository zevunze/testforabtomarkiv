﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using TestForAbto.Models;

namespace TestForAbto
{
    public class SponsorService
    {
        public List<int> GetSponsorsID()
        {
            List<int> sponsorIDs = new();

            const string connectionString = "Server=localhost\\SQLEXPRESS03;Database=TestForAbto;TrustServerCertificate=true;Trusted_Connection=true";
            string query = "SELECT ID FROM Sponsors;";

            using (SqlConnection connection = new(connectionString))
            {
                SqlCommand command = new(query, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int sponsorID = reader.GetInt32(0);
                        sponsorIDs.Add(sponsorID);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return sponsorIDs;
        }

        public SponsorModel GetSponsorInfo(int id)
        {
            SponsorModel sponsor = null;

            const string connectionString = "Server=localhost\\SQLEXPRESS03;Database=TestForAbto;TrustServerCertificate=true;Trusted_Connection=true";
			string query = "SELECT * FROM Sponsors WHERE ID = @ID";
			
			using (SqlConnection connection = new(connectionString))
			{
				SqlCommand command = new(query, connection);
                command.Parameters.AddWithValue("@ID", id);

                try
				{
					connection.Open();
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						int sponsorID = reader.GetInt32(0);

						sponsor = new()
						{
							ID = reader.GetInt32(0),
							FirstName = reader.GetString(1),
							LastName = reader.GetString(2),
							Email = reader.GetString(3),
							BirthDate = reader.GetDateTime(4),
							Note = reader.GetString(5)
						};
					}
					reader.Close();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}

			query = "SELECT ID FROM Childrens WHERE SponsorID = @ID";
			using (SqlConnection connection = new(connectionString))
            {
                SqlCommand command = new(query, connection);
                command.Parameters.AddWithValue("@ID", id);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while(reader.Read())
                    {
                        sponsor.Children = new();

						while (reader.Read())
						{
							int sponsorID = reader.GetInt32(0);
							sponsor.Children.Add(sponsorID);
						}
					}
                    

                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

			return sponsor;
        }

        public void Update(SponsorModel sponsor)
        {
			const string connectionString = "Server=localhost\\SQLEXPRESS03;Database=TestForAbto;TrustServerCertificate=true;Trusted_Connection=true";

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();

				string sqlQuery = "UPDATE Sponsors SET FirstName = @Value1, LastName = @Value2, Email = @Value3, BirthDate = @Value4, Note = @Value5 WHERE ID = @ID";
				using (SqlCommand command = new SqlCommand(sqlQuery, connection))
				{
					command.Parameters.AddWithValue("@Value1", sponsor.FirstName);
					command.Parameters.AddWithValue("@Value2", sponsor.LastName);
					command.Parameters.AddWithValue("@Value3", sponsor.Email);
					command.Parameters.AddWithValue("@Value4", sponsor.BirthDate.ToString("yyyy-MM-dd"));
					command.Parameters.AddWithValue("@Value5", sponsor.Note);

					command.Parameters.AddWithValue("@ID", sponsor.ID);

					command.ExecuteNonQuery();
				}
			}
		}
    }
}
